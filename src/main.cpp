#include <stdio.h>
#include "pico/stdlib.h"
#include "hardware/i2c.h" 

extern "C" {
#include "pico/bootrom.h"
}

#include <display_devices.hpp>
#include <framebuffer.hpp>

struct my_device_descr : sh1106_128_64_device {
    static constexpr unsigned port { 0x3c };
    static constexpr unsigned pin_sda { 4 };
    static constexpr unsigned pin_scl { 5 };
};
using myFramebuffer = framebuffer<my_device_descr, my_device_routines>;


int main() {
    stdio_init_all();
    const uint LED_PIN = 25;
    gpio_init(LED_PIN);
    gpio_set_dir(LED_PIN, GPIO_OUT);
    auto on = 100;
    auto off = 100;

    i2c_init(i2c0, 100 * 1000);

    myFramebuffer mfb;

    if(auto success = mfb.init(); success) {
        mfb.cls();

        mfb.drawBox(0,0, mfb.getWidth()-1, mfb.getHeight()-1, 1);
        mfb.drawLine(0,0,mfb.getWidth()-1, mfb.getHeight()-1, 1);
        mfb.drawLine(mfb.getWidth()-1, 0 , 0, mfb.getHeight()-1, 1);
        mfb.drawCircle(mfb.getWidth() /2 -1, mfb.getHeight() /2 -1, 30, 1);
        mfb.drawCircle(mfb.getWidth() /4 -1, mfb.getHeight() /4 -1, 10, 1);
        mfb.drawCircle(mfb.getWidth() -1, mfb.getHeight() -1, 40, 1);

        mfb.put_character(10,10, '4');
        mfb.put_string(2, 16, "Hello world!");

        if(mfb.present()) {
            off = 500;
            on = 500;
        }
    }
    sleep_ms(1000);
    
    unsigned repeat{20};
    unsigned start{0};
    while (repeat--) {
        sleep_ms(200);
//        gpio_put(LED_PIN, 1);
//        sleep_ms(on);
//        gpio_put(LED_PIN, 0);
//        sleep_ms(off);
//        mfb.cls();
//        mfb.write_glyph(start, 16, H_glyph);
        start += 4;
        mfb.present();
    }
    reset_usb_boot(0,0);

    return 0;
}
