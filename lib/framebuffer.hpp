#pragma once 
#include <string_view>
#include <array>
#include <cmath>
#include <cstdint>
#include "glyphs.hpp"

template<typename device_descriptor, template<typename> typename device_routines>
struct framebuffer {
    using routines = device_routines<device_descriptor>;
    using descriptor = device_descriptor;

    struct Pos {
        unsigned x{};
        unsigned y{};
    };
    using pixel_type = typename device_descriptor::pixel_type;
    static constexpr unsigned frame_size = 
        (device_descriptor::width * device_descriptor::height) >> 3;
    using frame_type = std::array<uint8_t, frame_size>;

    frame_type current_frame {};
    frame_type previous_frame {};


    bool init() {
        return routines::init();
    }

    void drawBitmap(int16_t x, int16_t y, uint8_t const * bitmap,
            int16_t w, int16_t h, pixel_type color) {
        auto bytewidth = (w + 7) / 8;
        uint8_t byte{};

        for(int16_t j{}; j < h; j++, y++) {
            for(int16_t i{}; i < w; i++) {
                if( i & 7 ) {
                    byte <<= 1;
                } else {
                    byte = current_frame[j * bytewidth + i / 8];
                }
                if(byte & 0x80) {
                    putPixel(x + i, y, color);
                }
            }
        }
    }

    constexpr unsigned getWidth() {
        return device_descriptor::width;
    }

    constexpr unsigned getHeight() {
        return device_descriptor::height;
    }

    void putPixel(int16_t x, int16_t y, pixel_type pixel) {
        if(x < 0 || y < 0 || x >= getWidth() || y >= getHeight()) {
            return;
        }

        auto const location = x + ((y >> 3) * getWidth());
    
        if(pixel) {
            current_frame[location] |= static_cast<uint8_t>(1 << (y&7));
        } else {
            current_frame[location] &= static_cast<uint8_t>(~(1 << (y&7)));
        }
    }

    void drawLine(int x0, int y0, int x1, int y1, pixel_type pixel) {
        // "low" angle
        auto const drawLineLow=[&](int x0, int y0, int x1, int y1, pixel_type pixel) {
            int dx = x1 - x0;
            int dy = y1 - y0;
            int yIncr = 1;

            if(dy < 0) {
                yIncr = -1;
                dy = -dy;
            }
            int d = (dy << 1) - dx;
            int y = y0;

            int incrLTE = dy << 1;
            int incrGT = (dy - dx) << 1;

            for(auto x{x0}; x < x1; x++) {
                putPixel(x, y, pixel);
                if(d > 0) {
                    y += yIncr;
                    d += incrGT;
                } else {
                    d += incrLTE;
                }
            }
        };
        // "high" angle
        auto const drawLineHigh=[&](int x0, int y0, int x1, int y1, pixel_type pixel) {
            int dx = x1 - x0;
            int dy = y1 - y0;
            int xIncr = 1;
            if(dx < 0) {
                xIncr = -1;
                dx = -dx;
            }

            int d = (dx << 1) - dy;
            int incrGT = dx << 1;
            int incrLTE = (dx - dy) << 1;
            int x = x0;

            for(auto y{y0}; y < y1; y++) {
                putPixel(x, y, pixel);
                if(d > 0) {
                    x += xIncr;
                    d += incrGT;
                } else {
                    d += incrLTE;
                }
            }
        };
        if(std::abs(y1 - y0) < std::abs(x1 - x0)) {
            if(x0 > x1) {
                drawLineLow(x1, y1, x0, y0, pixel);
            } else {
                drawLineLow(x0, y0, x1, y1, pixel);
            }
        } else {
            if(y0 > y1) {
                drawLineHigh(x1, y1, x0, y0, pixel);
            } else {
                drawLineHigh(x0, y0, x1, y1, pixel);
            }
        }
    }

    void drawBox(int x0, int y0, int x1, int y1, pixel_type pixel) {
        drawLine(x0, y0, x1, y0, pixel);
        drawLine(x1, y0, x1, y1, pixel);
        drawLine(x1, y1, x0, y1, pixel);
        drawLine(x0, y1, x0, y0, pixel);
    }

    void drawCircle(int origin_x, int origin_y, int radius, pixel_type pixel) {
        auto const circlePoints=[&](int origin_x, int origin_y, int x, int y, pixel_type pixel) {
            putPixel(origin_x + x, origin_y + y, pixel);
            putPixel(origin_x + y, origin_y + x, pixel);
            putPixel(origin_x + y, origin_y + -x, pixel);
            putPixel(origin_x + x, origin_y + -y, pixel);
            putPixel(origin_x + -x, origin_y + -y, pixel);
            putPixel(origin_x + -y, origin_y + -x, pixel);
            putPixel(origin_x + -y, origin_y + x, pixel);
            putPixel(origin_x + -x, origin_y + y, pixel);
        };

        int x{};
        int y{radius};
        int d = 1 - radius;
        circlePoints(origin_x, origin_y, x, y, pixel);

        while(y > x) {
            if(d < 0) {
                d += 2 * x + 3;
            } else { 
                d += 2 * (x - y)  + 5;
                y--;
            }
            x++;
            circlePoints(origin_x, origin_y, x, y, pixel);
        }
    }

    void cls() {
        current_frame.fill(0);
    }

    // use some of lefticus' logic for now.
    void write_glyph(const int x, const int y, glyph_type const &glyph) {
        // this is terribly wrong if we break assumptions
        std::size_t cur_col = 0;
        const auto row_offset = y % 8;
        for (const auto &byte : glyph) {
            const auto start_byte = (y / 8) * device_descriptor::width + x + cur_col;
            if (row_offset != 0) {
                current_frame[start_byte] |= (byte << row_offset);
                current_frame[start_byte + device_descriptor::width] |= (byte >> (7-row_offset));
            } else {
                current_frame[start_byte] = byte;
            }
            ++cur_col;
        }
    }

    void put_character(int x, int y, char c) {
        write_glyph(x, y, getGlyph(c));
    }

    void put_string(int x, int y, std::string_view const &text) {
        int count{};
        for(auto const c : text) {
            put_character(x + count, y, c);
            count+=8;
        }
    }

    // send to display
    bool present() {
        // check what needs to be send
        previous_frame = current_frame;
        // send data;
        return routines::copy(current_frame);
        // store frame
    }
};
