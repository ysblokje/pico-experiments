#pragma once

#include "pico/time.h"
#include <array>
#include <cstdint>
#include <algorithm>

#include <hardware/i2c.h>




enum ColorType {
    bpp_1,
    bpp_8,
};

struct dummy_256_256_device {
    static constexpr unsigned height {256};
    static constexpr unsigned width {256};
    static constexpr unsigned pages {8};
    static constexpr ColorType colortype{bpp_8};
    using pixel_type = uint8_t;
};

template<typename descriptor>
struct SDD1306 {
    enum command {
        memorymode=0x20,                        ///< See datasheet
        columnaddr=0x21,                        ///< See datasheet
        pageaddr=0x22,                          ///< See datasheet
        setcontrast=0x81,                       ///< See datasheet
        chargepump=0x8d,                        ///< See datasheet
        segremap=0xa0,                          ///< See datasheet
        displayallon_resume=0xa4,               ///< See datasheet
        displayallon=0xa5,                      ///< Not currently used
        normaldisplay=0xa6,                     ///< See datasheet
        invertdisplay=0xa7,                     ///< See datasheet
        setmultiplex=0xa8,                      ///< See datasheet
        displayoff=0xae,                        ///< See datasheet
        displayon=0xaf,                         ///< See datasheet
        comscaninc=0xc0,                        ///< Not currently used
        comscandec=0xc8,                        ///< See datasheet
        setdisplayoffset=0xd3,                  ///< See datasheet
        setdisplayclockdiv=0xd5,                ///< See datasheet
        setprecharge=0xd9,                      ///< See datasheet
        setcompins=0xda,                        ///< See datasheet
        setvcomdetect=0xdb,                     ///< See datasheet

        setlowcolumn=0x00,                      ///< Not currently used
        sethighcolumn=0x10,                     ///< Not currently used
        setstartline=0x40,                      ///< See datasheet

        externalvcc=0x01,                       ///< External display voltage source
        switchcapvcc=0x02,                      ///< Gen. display voltage from 3.3V

        right_horizontal_scroll=0x26,             ///< Init rt scroll
        left_horizontal_scroll=0x27,              ///< Init left scroll
        vertical_and_right_horizontal_scroll=0x29,///< Init diag scroll
        vertical_and_left_horizontal_scroll=0x2A, ///< Init diag scroll
        deactivate_scroll=0x2E,                   ///< Stop scroll
        activate_scroll=0x2F,                     ///< Start scroll
        set_vertical_scroll_area=0xA3,            ///< Set scroll rang
        control_reg = 0x0,
    };

    static constexpr const uint8_t init_commands[] = {
        displayoff,
        setdisplayclockdiv, 0xf0,
        setmultiplex, descriptor::height - 1,
        setdisplayoffset, 0x0,
        setstartline|0x0,

        segremap | 0x1,
        comscandec,
        setcompins, 0x12,
        setcontrast, 0x8f,
        chargepump, 0x14,
        memorymode, 0x0,
        setprecharge, 0xf1,
        setvcomdetect, 0x40,
        displayallon_resume,
        normaldisplay,
        deactivate_scroll,
        displayon,
    };
};


struct sh1106_128_64_device : SDD1306<sh1106_128_64_device> {
    static constexpr unsigned height {64};
    static constexpr unsigned width {128};
    static constexpr unsigned pages { height >> 3};
    static constexpr ColorType colortype{bpp_1};
    using pixel_type = bool;
};




template<typename device_descriptor>
struct my_device_routines {
    using my_descr = device_descriptor;
    using error = enum { 
        NO_ERROR,
        READ_FAILED,
        WRITE_FAILED,
    };

    template<size_t size>
    static std::array<uint8_t, size + 1> getDataBuffer() {
        std::array<uint8_t, size + 1> retval { 0x40 };
        return retval;

    }

    static void write_byte(uint8_t byte) {
            auto ret = i2c_write_blocking(i2c0, my_descr::port, &byte, 1, false);
    }
    
    template<typename cmd_type>
    static void send_commands(cmd_type const &cmds) {
        for(auto const &c : cmds) {
            uint8_t buffer[2]{my_descr::control_reg, c};
            auto ret = i2c_write_blocking(i2c0, my_descr::port, buffer, 2, false);
        }
    }
    template<size_t size>
    static void send_commands(uint8_t const (&cmds)[size]) {
        for(auto const &c : cmds) {
            uint8_t buffer[2]{my_descr::control_reg, c};
            auto ret = i2c_write_blocking(i2c0, my_descr::port, buffer, 2, false);
        }
    }

    static void send_data(uint8_t const *data, size_t data_size) {
        uint8_t buffer[16] { 0x40 };
        std::copy(data, data + data_size, &buffer[1]);
        auto ret = i2c_write_blocking(i2c0, my_descr::port, buffer, ++data_size, false);
    }

    static void send_data(uint8_t const data) {
        return send_data(&data, 1);
    }

    static constexpr bool reserved_addr(uint8_t addr) {
        return (addr & 0x78) == 0 || (addr & 0x78) == 0x78;
    }

    static bool init() {
        uint8_t rxdata{};
        if(!reserved_addr(my_descr::port)) {
            gpio_set_function(my_descr::pin_sda, GPIO_FUNC_I2C);
            gpio_pull_up(my_descr::pin_sda);
            gpio_set_function(my_descr::pin_scl, GPIO_FUNC_I2C);
            gpio_pull_up(my_descr::pin_scl);
            auto size = sizeof(my_descr::init_commands);
            send_commands(my_descr::init_commands);
            sleep_ms(100);
            return true;
        } 
        return false;
    }

    static void sendPage(unsigned int page) {
        if(page >= (my_descr::height >> 3)) {
            return;
        }
    }

    template<typename container_type>
    static bool copy(container_type const &data) {
        bool retval {true};
        send_commands({ 
                my_descr::columnaddr, 0, my_descr::width-1,
                my_descr::pageaddr, 0x0, my_descr::pages-1,
                });

        uint8_t buffer[16];
        buffer[0] = 0x40;
        auto data_begin { data.begin() };
        auto const data_end { data.end() };

        while(data_begin != data_end) {
            auto const distance = std::min(std::distance(data_begin, data_end), 15);
            std::copy(data_begin, std::next(data_begin, distance), buffer+1);
            auto written = i2c_write_blocking(i2c0, my_descr::port, buffer, distance+1, false);
            std::advance(data_begin, distance);
        }
        return true;
    }
};
