#pragma once
#include <array>
#include <cstdint>
#include "font8x8_basic.h"

using glyph_type = std::array<uint8_t, 8>;

// see https://github.com/lefticus/i2c_hacking/pull/4
constexpr auto transpose(glyph_type const& input) {
    glyph_type output{};

    for(size_t row_out = 0 ; row_out < 8 ; ++row_out) {
        for(size_t row_in = 0 ; row_in < 8; ++row_in) {
            output[row_out] |= (input[row_in] >> (row_out) & 1) << row_in;
        }
    }
    return output;
}

template<std::size_t START, std::size_t UNTIL> 
constexpr auto const create_glyph_array() {
    std::array<glyph_type, UNTIL-START> retval{};
    for(auto i{START}; i < UNTIL; i++) {
        retval[i-START] = transpose(std::to_array(font8x8_basic[i]));
    }
    return retval;
}

static constexpr auto glyphs {
    create_glyph_array<32,128>()
};

glyph_type const &getGlyph(char c) {
    return glyphs[c-32];
}
